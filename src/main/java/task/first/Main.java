package task.first;

import java.math.BigInteger;
import java.util.Scanner;

// Имеет ограничени на входные данные. Число не должно быть больше 85307129 (приблизительный расчет).
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextBigInteger()) {
            BigInteger number = scanner.nextBigInteger();
            BigInteger factorial = factorial(number);
            System.out.println("Zero count:" + countZero(factorial));
        }
    }

    static long countZero(BigInteger bigInteger) {
        String bigIntegerString = bigInteger.toString();
        return bigIntegerString.chars().filter(e -> e == 48).count();
    }

    static BigInteger factorial(BigInteger number) {
        if (number.equals(BigInteger.ZERO)) {
            return BigInteger.ONE;
        }
        if (number.compareTo(BigInteger.ZERO) < 0) {
            System.out.println("");
            throw new IllegalArgumentException();
        }
        BigInteger result = BigInteger.ONE;
        BigInteger i = BigInteger.ZERO;
        while (number.compareTo(i) > 0) {
            i = i.add(BigInteger.ONE);
            result = result.multiply(i);
        }
        return result;
    }
}
