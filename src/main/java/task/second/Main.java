package task.second;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextBigInteger()) {
            BigInteger number = scanner.nextBigInteger();
            System.out.println("Zero count:" + countZero(number).toString());
        }
    }

    static BigInteger countZero(BigInteger number) {
        if (BigInteger.ZERO.equals(number)) {
            return BigInteger.ZERO;
        }
        if (number.compareTo(BigInteger.ZERO) < 0) {
            System.out.println("");
            throw new IllegalArgumentException();
        }
        BigInteger result = BigInteger.ZERO;
        BigInteger i = BigInteger.ONE;
        while (number.compareTo(BigInteger.ZERO) > 0) {
            number = number.divide(BigInteger.valueOf(5));
            result = result.add(number);
        }
        return result;
    }
}
